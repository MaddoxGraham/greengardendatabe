-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 15, 2022 at 02:23 PM
-- Server version: 5.7.33
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `greengarden`
--

--
-- Dumping data for table `categorie`
--

INSERT INTO `categorie` (`idCat`, `nomination`) VALUES
(1, 'été'),
(2, 'hiver'),
(3, 'automne'),
(4, 'printemps'),
(5, 'interieur'),
(6, 'exterieur'),
(7, 'Outils de jardin'),
(8, 'Decorations');

--
-- Dumping data for table `client`
--

INSERT INTO `client` (`idClient`, `nom`, `prenom`, `adresse`, `ville`, `cp`, `mail`, `télephone`, `login`, `pwd`, `newsletter`, `nomination`, `idCommercial`) VALUES
(1, 'Love', 'Lee', '451-9825 Mi Avenue', 'Oaxaca', 5115, 'dolor@aol.org', '08 18 04 29 84', 'LeeLov', 'd8530f2c31b4a7234ea5f13d88269061dcf927e2', 0, 'particulier', 3),
(2, 'Waters', 'Vicky', 'Ap #568-4786 Euismod St.', 'Korosten', 792458, 'Waters.Vicky@icloud.org', '09 12 94 84 36', 'WatersVicky', 'e7b61e57d1f2f263d89ca12f7eb5f942626acfbf', 0, 'particulier', 3),
(3, 'Gould', 'Stewart', 'P.O. Box 604, 1158 Eros St.', 'Korosten', 792458, 'consectetuer@icloud.org', '04 88 57 60 43', 'Sed', 'd074f92dee5349a750e07fcc19320dc89072d4a6', 1, 'professionnel', 1),
(4, 'Fuentes', 'Jerry', 'P.O. Box 660, 2724 Ipsum. Rd.', 'Cartagena', 81805, 'vitae@google.ca', '07 32 56 93 55', 'malesuada', '7f05281d5d381f3ab8f08482f3dd66d622476b63', 1, 'particulier', 3),
(5, 'Fowler', 'Patrick', 'Ap #267-7423 Augue Av.', 'Göteborg', 166844, 'est.mollis@hotmail.com', '03 70 02 66 26', 'consectetuer', '9ac73f1e7126486a9cd321d96ed065dd206a2e1f', 0, 'particulier', 4);

--
-- Dumping data for table `coefficient`
--

INSERT INTO `coefficient` (`nomination`) VALUES
('particulier'),
('professionnel');

--
-- Dumping data for table `commande`
--

INSERT INTO `commande` (`idCom`, `dateCom`, `idClient`, `reduction`, `TotalTTC`, `statut`) VALUES
(1, '2022-03-14 00:00:00', 1, 0, '160.00', 'saisie');

--
-- Dumping data for table `commercial`
--

INSERT INTO `commercial` (`idCommercial`, `nom`, `prenom`) VALUES
(1, 'Goudonnet', 'Jonathan'),
(2, 'Pac', 'Pierre'),
(3, 'Raimbault', 'Diane'),
(4, 'Lopez', 'Kévin');

--
-- Dumping data for table `contienirproduit`
--

INSERT INTO `contienirproduit` (`RefProduit`, `reference`) VALUES
(108, 1),
(113, 2);

--
-- Dumping data for table `facture`
--

INSERT INTO `facture` (`numeroFact`, `statut`, `datePayement`, `idCom`, `dateCom`) VALUES
(1, 'impayé', '2022-03-15', 1, '2022-03-14 00:00:00');

--
-- Dumping data for table `fournisseur`
--

INSERT INTO `fournisseur` (`RefFournisseur`, `nomination`) VALUES
(1011998578, 'LES CACTUS DE CHEZ JAMIN'),
(1020014253, ' Horticulture F. GOUIN\r\n'),
(1032025689, ' JEAN-ANDRE AUDISSOU\r\n'),
(1041545526, ' LEPAGE VIVACES\r\n'),
(1254671256, 'Ets. Kuentz');

--
-- Dumping data for table `lignecommande`
--

INSERT INTO `lignecommande` (`reference`, `designation`, `prixHT`, `quantite`, `idCom`, `dateCom`) VALUES
(1, 'Kit jardin ', '30.00', 1, 1, '2022-03-14 00:00:00'),
(2, 'Palmm', '130.00', 1, 1, '2022-03-14 00:00:00');

--
-- Dumping data for table `produit`
--

INSERT INTO `produit` (`RefProduit`, `shortLibel`, `src`, `srcVid`, `longLibel`, `prxAchat`, `idCat`) VALUES
(100, 'Dalhia jaune', 'https://www.visoflora.com/images/original/dahlia-jaune-visoflora-36606.jpg', NULL, 'Dalhia Imperium couleur jaune a planter soi même. Prix du bulbe à l\'unitée.', '3.50', 6),
(101, 'Cactus Grande Taille', 'https://images.pexels.com/photos/4507263/pexels-photo-4507263.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', '../images/plant006_vid.mp4', 'Cactus d\'interieur/exterieur 26cm de haut', '26.00', 5),
(102, 'Presentoir interieur', '../images/plant009.png', '../images/plant_009_vid.mp4', 'décorez votre intérieur avec ce présentoir modern pour plantes. Pots compris dans l\'offre cactus non inclus.', '10.00', 8),
(103, 'Espostoa', '../images/plant002.png', NULL, 'Espostoa est un genre de la famille des cactus composé de seize espèces.Ce sont des cierges arbustifs ou arborescents couverts d\'épines denses et portant souvent des poils ou des cheveux blancs. Seuls les spécimens âgés peuvent se ramifier.', '6.99', 6),
(104, 'Asplenium antiquum', '../images/plant001.jpeg', '../images/plante001_v2.jpg', 'Ces fougères prospèrent bien aux températures habituelles d\'une pièce d\'habitation. N\'exposez pas Asplenium nidus à des températures inférieures à 15° en hiver et les autres espèces, au-dessous de 10°.', '32.50', 6),
(105, 'Cisaille à haie', '../images/CisailleHaie.jpeg', NULL, 'Cisaille à haies spécialement conçue pour l\'usage professionnel en espaces verts, domaines viticoles et particulièrement pour l\'art topiaire\r\nLame entièrement trempée\r\nLame crantée pour la coupe de grosses branches, jusqu\'à 10 mm de diamètre', '58.00', 7),
(106, 'Petit Kit de jardinage', '../images/KitOutils.jpeg', '../images/KitOutils_V2.jpeg', 'Kit de jardinage pour débutants.Contenant une petite pelle, une griffe et une serfouette avec poignée en bois ergonomique, vous pourrez réaliser vos travaux de jardin tout en profitant d\'un outillage de qualité vous offrant un réél confort d\'utilisation.', '25.00', 7),
(107, 'Cisaille à main', 'https://images.pexels.com/photos/5027604/pexels-photo-5027604.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', '../images/cisailleMain.mp4', 'Coupez tout type de haies avec l\'aide de cette cisaille Bahco P59-25-F universelle.\r\n\r\nRobuste et fiable, elle peut être utilisée régulièrement afin d\'entretenir votre jardin avec efficacité.\r\n\r\nCet outil possède une lame en acier trempé et des butée en caoutchouc afin de travailler sans difficulté.', '33.25', 7),
(108, 'Kit jardin ', 'https://images.pexels.com/photos/5218008/pexels-photo-5218008.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', NULL, 'Les enfants sont naturellement très ouverts et curieux de découvrir la magie de la nature. Pour les aider à se lancer dans une activité de jardinage, voici le kit idéal pour les équiper d\'outils de jardinage adaptés à leur morphologie.', '30.00', 7),
(109, 'Roses Rouges', 'https://images.pexels.com/photos/1252812/pexels-photo-1252812.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', '../images/rose_VId.mp4', 'Ncient 50 pcs/Sac Graines Semences de Rose Couleur Rouge de Graines Fleurs Graines à Planter Plante Rare de Jardin Balcon Vivaces Belle Floraison Bonsaï en Plein Air pour l\'Intérieur et l\'Extérieur', '10.99', 6),
(110, 'Nopal', '../images/plant006.jpeg', NULL, 'Nopal est un nom commun en espagnol pour les cactus Opuntia, ainsi que pour ses coussinets. Il existe environ 114 espèces connues au Mexique', '70.00', 6),
(111, 'Epipremnum aureum', '../images/plant004.jpeg', 'https://media.istockphoto.com/videos/rain-on-evening-video-id1329649492', 'La plante Epipremnum aureum est très populaire dans les régions tempérées pour sa facilité de culture qui ne demande quasiment aucun entretien', '13.00', 5),
(112, 'Pot noir modern', 'https://images.pexels.com/photos/4273435/pexels-photo-4273435.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260', NULL, 'Pot moderne noir pour plante d\'interieur', '11.50', 8),
(113, 'Palm', 'https://img.made.com/image/upload/c_pad,d_madeplusgrey.svg,f_auto,w_982,dpr_2.0,q_auto:good,b_rgb:f5f6f4/v4/catalog/product/asset/c/1/c/9/c1c92985f59ad81056038380c2d1d99b8779e66b_IACPAL003GRE_UK_Palm_Set_Two_Large_Earthenware_Plant_Stands_Sage_Pink_ar3_2_LB01_PS.png', 'https://img.made.com/image/upload/c_pad,d_madeplusgrey.svg,f_auto,w_982,dpr_2.0,q_auto:good,b_rgb:f5f6f4/v4/catalog/product/asset/d/5/6/1/d561e91f7e9ef48e425a93a7bb4a09fed766b554_IACPAL003GRE_UK_Palm_Set_Two_Large_Earthenware_Plant_Stands_Sage_Pink_ar3_2_LB02_LS.jpg', 'Lot de 2 grands pots de fleurs sur pieds, vert sauge et rose', '130.00', 8),
(114, 'Umbra Trigg Jardinière Suspendue', 'https://m.media-amazon.com/images/I/71zFkdcesJL._AC_SX679_.jpg', 'https://m.media-amazon.com/images/I/71-kaFXpyxL._AC_SX679_.jpg', 'SUPERBE DESIGN MODERNE ET GEOMETRIQUE : Trigg est un pot de fleur mural avec support géométrique original, à la fois simple et élégant.\r\nMAGNIFIQUE POT DE FLEURS : Trigg est la solution idéale pour introduire de petites plantes d\'intérieur, telles que des plantes succulentes, des plantes aériennes et autres chez vous ou au bureau\r\nLÉGER ET POLYVALENT: Trigg peut également être utilisé pour stocker de petits objets comme des stylos, des crayons et même des accessoires de maquillage', '24.00', 8),
(115, 'Donec molestie', 'Phasellus tristique mauris sed', 'volutpat malesuada. Mauris ', 'aliquam scelerisque urna at mattis. Donec molestie magna leo, sed eleifend est mollis id', '8.99', 6),
(116, 'Terrarium en verre forme géométrique', 'https://m.media-amazon.com/images/I/91tfQ65HLPL._AC_SX679_.jpg', 'https://m.media-amazon.com/images/I/71Qddbj2AbL._AC_SX679_.jpg', 'Style moderne terrarium avec élégance classique, un joli cube en verre artistique d\'une pièce de votre maison ou votre terrasse.Taille idéale pour la fougère, mousse, succulente, airplants, cactus ou d\'autres plantes avec facile maintainence', '41.25', 8),
(117, 'Cache-pot sur pied', 'https://cdn.laredoute.com/products/0/e/1/0e1723de8770ff303c1e64ad7acc2a2c.jpg?imgopt=twic&twic=v1/cover=700x700', 'https://cdn.laredoute.com/products/7/d/d/7dd10c78a7c937eaaf38a0a41d0bbadc.jpg?imgopt=twic&twic=v1/cover=700x700', '2 cache-pots en métal. Chacun doté de son piédestal, En raison du processus de production, il y a un petit trou de quelques mm de taille sur le bord supérieur.', '66.00', 8),
(118, 'Euphorbia ammak', '../images/plant010.jpeg', '../images/plant010_vid.mp4', 'Cactus exterieur grande taille. 38cm environ. ', '40.00', 6),
(119, 'Voile d\'hivernage', 'https://photos.gammvert.fr/v5/products/fiche_590/52281-voile-dhivernage-nortene-wintertex-2-x-5-m-2.jpg', NULL, 'Ce voile d’hivernage de la marque Nortene protège les plantes du froid et du vent. Il est conçu pour tous types de plantes. Son voile est perméable à l\'eau et à l\'air. Il est un bon isolant thermique. Il est également facile à mettre en place.\r\n', '15.00', 6),
(120, 'Voile d\'hivernage', 'https://photos.gammvert.fr/v5/products/fiche_590/52286-voile-dhivernage-nortene-hivertex-2-x-5-m-2.jpg', 'https://photos.gammvert.fr/v5/products/fiche_590/52288-voile-dhivernage-nortene-hivertex-4-x-6-m-3.jpg', 'Ce voile d’hivernage Hivertex de la marque Nortene est un voile épais pour une protection renforcée contre le froid. Il est perméable à l’eau et à l’air. Ainsi, vous pourrez protéger vos plantes du froid l’hiver.', '19.00', 6),
(121, 'Coupe branches télescopique', 'https://photos.gammvert.fr/v5/products/fiche_590/50626-coupe-branches-multifonctions-upx86-scie-fiskars-3.JPG', 'https://photos.gammvert.fr/v5/products/fiche_590/50626-coupe-branches-multifonctions-upx86-scie-fiskars-6.jpg', 'Diamètre de coupe : Ø 32 mm\r\nPortée de coupe d\'environ 3,5 m\r\nTête de coupe réglable jusqu\'à 230°', '109.00', 7),
(122, 'Palmier de Madagascar', '../images/palmierMadagascar.jpeg', NULL, 'Palmier en pot 1.20m ', '100.00', 6);

--
-- Dumping data for table `stocker`
--

INSERT INTO `stocker` (`RefProduit`, `RefFournisseur`, `quantite`) VALUES
(100, 1020014253, 30),
(101, 1011998578, 50),
(102, 1254671256, 100),
(103, 1011998578, 50),
(104, 1032025689, 60),
(105, 1020014253, 14),
(106, 1041545526, 25),
(107, 1020014253, 10),
(108, 1032025689, 20),
(109, 1041545526, 105),
(110, 1254671256, 80),
(111, 1011998578, 70),
(112, 1254671256, 30),
(113, 1020014253, 20),
(114, 1254671256, 54),
(115, 1041545526, 60),
(116, 1032025689, 35),
(117, 1254671256, 50),
(118, 1032025689, 20),
(119, 1011998578, 200),
(120, 1020014253, 150),
(121, 1020014253, 30),
(122, 1254671256, 25);

-- --------------------------------------------------------

--
-- Structure for view `produits_fournisseur`
--
DROP TABLE IF EXISTS `produits_fournisseur`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `produits_fournisseur`  AS  select `produit`.`RefProduit` AS `RefProduit`,`produit`.`shortLibel` AS `nomProduit`,`produit`.`longLibel` AS `LongLibel`,`produit`.`prxAchat` AS `prxAchat`,`produit`.`src` AS `src`,`produit`.`srcVid` AS `srcVid`,`produit`.`idCat` AS `idCat`,`fournisseur`.`RefFournisseur` AS `RefFournisseur`,`fournisseur`.`nomination` AS `nomFournisseur` from ((`produit` join `fournisseur`) join `stocker`) where ((`produit`.`RefProduit` = `stocker`.`RefProduit`) and (`fournisseur`.`RefFournisseur` = `stocker`.`RefFournisseur`)) ;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
