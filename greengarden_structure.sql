-- phpMyAdmin SQL Dump
-- version 4.8.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost:3306
-- Generation Time: Mar 15, 2022 at 02:24 PM
-- Server version: 5.7.33
-- PHP Version: 7.4.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `greengarden`
--

-- --------------------------------------------------------

--
-- Table structure for table `appartenircat`
--

CREATE TABLE `appartenircat` (
  `idCat` int(11) NOT NULL,
  `idCat_1` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `categorie`
--

CREATE TABLE `categorie` (
  `idCat` int(11) NOT NULL,
  `nomination` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `client`
--

CREATE TABLE `client` (
  `idClient` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `adresse` varchar(50) DEFAULT NULL,
  `ville` varchar(50) DEFAULT NULL,
  `cp` int(11) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `télephone` varchar(50) DEFAULT NULL,
  `login` varchar(50) DEFAULT NULL,
  `pwd` varchar(50) DEFAULT NULL,
  `newsletter` tinyint(1) DEFAULT NULL,
  `nomination` varchar(50) NOT NULL,
  `idCommercial` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `coefficient`
--

CREATE TABLE `coefficient` (
  `nomination` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `commande`
--

CREATE TABLE `commande` (
  `idCom` int(11) NOT NULL,
  `dateCom` datetime NOT NULL,
  `idClient` int(11) NOT NULL DEFAULT '1',
  `reduction` int(11) DEFAULT NULL,
  `TotalTTC` decimal(5,2) DEFAULT NULL,
  `statut` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `commercial`
--

CREATE TABLE `commercial` (
  `idCommercial` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `contienirproduit`
--

CREATE TABLE `contienirproduit` (
  `RefProduit` int(11) NOT NULL,
  `reference` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `facture`
--

CREATE TABLE `facture` (
  `numeroFact` int(11) NOT NULL,
  `statut` varchar(50) DEFAULT NULL,
  `datePayement` date DEFAULT NULL,
  `idCom` int(11) NOT NULL,
  `dateCom` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `fournisseur`
--

CREATE TABLE `fournisseur` (
  `RefFournisseur` int(11) NOT NULL,
  `nomination` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `historique`
--

CREATE TABLE `historique` (
  `idClient` int(11) NOT NULL,
  `idCom` int(11) NOT NULL,
  `dateCom` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `lignecommande`
--

CREATE TABLE `lignecommande` (
  `reference` int(11) NOT NULL,
  `designation` varchar(50) DEFAULT NULL,
  `prixHT` decimal(5,2) DEFAULT NULL,
  `quantite` int(11) DEFAULT NULL,
  `idCom` int(11) NOT NULL,
  `dateCom` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `livraison`
--

CREATE TABLE `livraison` (
  `idLiv` int(11) NOT NULL,
  `dateLivraison` date DEFAULT NULL,
  `bonlivraison` varchar(50) DEFAULT NULL,
  `numeroFact` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Table structure for table `produit`
--

CREATE TABLE `produit` (
  `RefProduit` int(11) NOT NULL,
  `shortLibel` varchar(100) DEFAULT NULL,
  `src` varchar(500) NOT NULL,
  `srcVid` varchar(500) DEFAULT NULL,
  `longLibel` varchar(500) DEFAULT NULL,
  `prxAchat` decimal(5,2) NOT NULL,
  `idCat` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Stand-in structure for view `produits_fournisseur`
-- (See below for the actual view)
--
CREATE TABLE `produits_fournisseur` (
`RefProduit` int(11)
,`nomProduit` varchar(100)
,`LongLibel` varchar(500)
,`prxAchat` decimal(5,2)
,`src` varchar(500)
,`srcVid` varchar(500)
,`idCat` int(11)
,`RefFournisseur` int(11)
,`nomFournisseur` varchar(255)
);

-- --------------------------------------------------------

--
-- Table structure for table `stocker`
--

CREATE TABLE `stocker` (
  `RefProduit` int(11) NOT NULL,
  `RefFournisseur` int(11) NOT NULL,
  `quantite` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- --------------------------------------------------------

--
-- Structure for view `produits_fournisseur`
--
DROP TABLE IF EXISTS `produits_fournisseur`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `produits_fournisseur`  AS  select `produit`.`RefProduit` AS `RefProduit`,`produit`.`shortLibel` AS `nomProduit`,`produit`.`longLibel` AS `LongLibel`,`produit`.`prxAchat` AS `prxAchat`,`produit`.`src` AS `src`,`produit`.`srcVid` AS `srcVid`,`produit`.`idCat` AS `idCat`,`fournisseur`.`RefFournisseur` AS `RefFournisseur`,`fournisseur`.`nomination` AS `nomFournisseur` from ((`produit` join `fournisseur`) join `stocker`) where ((`produit`.`RefProduit` = `stocker`.`RefProduit`) and (`fournisseur`.`RefFournisseur` = `stocker`.`RefFournisseur`)) ;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `appartenircat`
--
ALTER TABLE `appartenircat`
  ADD PRIMARY KEY (`idCat`,`idCat_1`),
  ADD KEY `idCat_1` (`idCat_1`);

--
-- Indexes for table `categorie`
--
ALTER TABLE `categorie`
  ADD PRIMARY KEY (`idCat`);

--
-- Indexes for table `client`
--
ALTER TABLE `client`
  ADD PRIMARY KEY (`idClient`),
  ADD KEY `nomination` (`nomination`),
  ADD KEY `idCommercial` (`idCommercial`);

--
-- Indexes for table `coefficient`
--
ALTER TABLE `coefficient`
  ADD PRIMARY KEY (`nomination`);

--
-- Indexes for table `commande`
--
ALTER TABLE `commande`
  ADD PRIMARY KEY (`idCom`,`dateCom`),
  ADD KEY `idClient` (`idClient`);

--
-- Indexes for table `commercial`
--
ALTER TABLE `commercial`
  ADD PRIMARY KEY (`idCommercial`);

--
-- Indexes for table `contienirproduit`
--
ALTER TABLE `contienirproduit`
  ADD PRIMARY KEY (`RefProduit`,`reference`),
  ADD KEY `reference` (`reference`);

--
-- Indexes for table `facture`
--
ALTER TABLE `facture`
  ADD PRIMARY KEY (`numeroFact`),
  ADD UNIQUE KEY `idCom` (`idCom`,`dateCom`);

--
-- Indexes for table `fournisseur`
--
ALTER TABLE `fournisseur`
  ADD PRIMARY KEY (`RefFournisseur`);

--
-- Indexes for table `historique`
--
ALTER TABLE `historique`
  ADD PRIMARY KEY (`idClient`,`idCom`,`dateCom`),
  ADD KEY `idCom` (`idCom`,`dateCom`);

--
-- Indexes for table `lignecommande`
--
ALTER TABLE `lignecommande`
  ADD PRIMARY KEY (`reference`),
  ADD KEY `idCom` (`idCom`,`dateCom`);

--
-- Indexes for table `livraison`
--
ALTER TABLE `livraison`
  ADD PRIMARY KEY (`idLiv`),
  ADD KEY `numeroFact` (`numeroFact`);

--
-- Indexes for table `produit`
--
ALTER TABLE `produit`
  ADD PRIMARY KEY (`RefProduit`),
  ADD KEY `idCat` (`idCat`);

--
-- Indexes for table `stocker`
--
ALTER TABLE `stocker`
  ADD PRIMARY KEY (`RefProduit`,`RefFournisseur`),
  ADD KEY `RefFournisseur` (`RefFournisseur`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `appartenircat`
--
ALTER TABLE `appartenircat`
  ADD CONSTRAINT `appartenircat_ibfk_1` FOREIGN KEY (`idCat`) REFERENCES `categorie` (`idCat`),
  ADD CONSTRAINT `appartenircat_ibfk_2` FOREIGN KEY (`idCat_1`) REFERENCES `categorie` (`idCat`);

--
-- Constraints for table `client`
--
ALTER TABLE `client`
  ADD CONSTRAINT `client_ibfk_1` FOREIGN KEY (`nomination`) REFERENCES `coefficient` (`nomination`),
  ADD CONSTRAINT `client_ibfk_2` FOREIGN KEY (`idCommercial`) REFERENCES `commercial` (`idCommercial`);

--
-- Constraints for table `commande`
--
ALTER TABLE `commande`
  ADD CONSTRAINT `commande_ibfk_1` FOREIGN KEY (`idClient`) REFERENCES `client` (`idClient`);

--
-- Constraints for table `contienirproduit`
--
ALTER TABLE `contienirproduit`
  ADD CONSTRAINT `contienirproduit_ibfk_1` FOREIGN KEY (`RefProduit`) REFERENCES `produit` (`RefProduit`),
  ADD CONSTRAINT `contienirproduit_ibfk_2` FOREIGN KEY (`reference`) REFERENCES `lignecommande` (`reference`);

--
-- Constraints for table `facture`
--
ALTER TABLE `facture`
  ADD CONSTRAINT `facture_ibfk_1` FOREIGN KEY (`idCom`,`dateCom`) REFERENCES `commande` (`idCom`, `dateCom`);

--
-- Constraints for table `historique`
--
ALTER TABLE `historique`
  ADD CONSTRAINT `historique_ibfk_1` FOREIGN KEY (`idClient`) REFERENCES `client` (`idClient`),
  ADD CONSTRAINT `historique_ibfk_2` FOREIGN KEY (`idCom`,`dateCom`) REFERENCES `commande` (`idCom`, `dateCom`);

--
-- Constraints for table `lignecommande`
--
ALTER TABLE `lignecommande`
  ADD CONSTRAINT `lignecommande_ibfk_1` FOREIGN KEY (`idCom`,`dateCom`) REFERENCES `commande` (`idCom`, `dateCom`);

--
-- Constraints for table `livraison`
--
ALTER TABLE `livraison`
  ADD CONSTRAINT `livraison_ibfk_1` FOREIGN KEY (`numeroFact`) REFERENCES `facture` (`numeroFact`);

--
-- Constraints for table `produit`
--
ALTER TABLE `produit`
  ADD CONSTRAINT `produit_ibfk_1` FOREIGN KEY (`idCat`) REFERENCES `categorie` (`idCat`);

--
-- Constraints for table `stocker`
--
ALTER TABLE `stocker`
  ADD CONSTRAINT `stocker_ibfk_1` FOREIGN KEY (`RefProduit`) REFERENCES `produit` (`RefProduit`),
  ADD CONSTRAINT `stocker_ibfk_2` FOREIGN KEY (`RefFournisseur`) REFERENCES `fournisseur` (`RefFournisseur`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
