-- MySQL dump 10.13  Distrib 5.7.33, for Win64 (x86_64)
--
-- Host: localhost    Database: greengarden
-- ------------------------------------------------------
-- Server version	5.7.33

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Current Database: `greengarden`
--

CREATE DATABASE /*!32312 IF NOT EXISTS*/ `greengarden` /*!40100 DEFAULT CHARACTER SET utf8 */;

USE `greengarden`;

--
-- Table structure for table `appartenircat`
--

DROP TABLE IF EXISTS `appartenircat`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `appartenircat` (
  `idCat` int(11) NOT NULL,
  `idCat_1` int(11) NOT NULL,
  PRIMARY KEY (`idCat`,`idCat_1`),
  KEY `idCat_1` (`idCat_1`),
  CONSTRAINT `appartenircat_ibfk_1` FOREIGN KEY (`idCat`) REFERENCES `categorie` (`idCat`),
  CONSTRAINT `appartenircat_ibfk_2` FOREIGN KEY (`idCat_1`) REFERENCES `categorie` (`idCat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `appartenircat`
--

LOCK TABLES `appartenircat` WRITE;
/*!40000 ALTER TABLE `appartenircat` DISABLE KEYS */;
/*!40000 ALTER TABLE `appartenircat` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categorie`
--

DROP TABLE IF EXISTS `categorie`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categorie` (
  `idCat` int(11) NOT NULL,
  `nomination` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idCat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categorie`
--

LOCK TABLES `categorie` WRITE;
/*!40000 ALTER TABLE `categorie` DISABLE KEYS */;
INSERT INTO `categorie` VALUES (1,'été'),(2,'hiver'),(3,'automne'),(4,'printemps'),(5,'interieur'),(6,'exterieur'),(7,'Outils de jardin'),(8,'Decorations');
/*!40000 ALTER TABLE `categorie` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `client`
--

DROP TABLE IF EXISTS `client`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `client` (
  `idClient` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  `adresse` varchar(50) DEFAULT NULL,
  `ville` varchar(50) DEFAULT NULL,
  `cp` int(11) DEFAULT NULL,
  `mail` varchar(50) DEFAULT NULL,
  `télephone` varchar(50) DEFAULT NULL,
  `login` varchar(50) DEFAULT NULL,
  `pwd` varchar(50) DEFAULT NULL,
  `newsletter` tinyint(1) DEFAULT NULL,
  `nomination` varchar(50) NOT NULL,
  `idCommercial` int(11) NOT NULL,
  PRIMARY KEY (`idClient`),
  KEY `nomination` (`nomination`),
  KEY `idCommercial` (`idCommercial`),
  CONSTRAINT `client_ibfk_1` FOREIGN KEY (`nomination`) REFERENCES `coefficient` (`nomination`),
  CONSTRAINT `client_ibfk_2` FOREIGN KEY (`idCommercial`) REFERENCES `commercial` (`idCommercial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `client`
--

LOCK TABLES `client` WRITE;
/*!40000 ALTER TABLE `client` DISABLE KEYS */;
INSERT INTO `client` VALUES (1,'Love','Lee','451-9825 Mi Avenue','Oaxaca',5115,'dolor@aol.org','08 18 04 29 84','LeeLov','d8530f2c31b4a7234ea5f13d88269061dcf927e2',0,'particulier',3),(2,'Waters','Vicky','Ap #568-4786 Euismod St.','Korosten',792458,'Waters.Vicky@icloud.org','09 12 94 84 36','WatersVicky','e7b61e57d1f2f263d89ca12f7eb5f942626acfbf',0,'particulier',3),(3,'Gould','Stewart','P.O. Box 604, 1158 Eros St.','Korosten',792458,'consectetuer@icloud.org','04 88 57 60 43','Sed','d074f92dee5349a750e07fcc19320dc89072d4a6',1,'professionnel',1),(4,'Fuentes','Jerry','P.O. Box 660, 2724 Ipsum. Rd.','Cartagena',81805,'vitae@google.ca','07 32 56 93 55','malesuada','7f05281d5d381f3ab8f08482f3dd66d622476b63',1,'particulier',3),(5,'Fowler','Patrick','Ap #267-7423 Augue Av.','Göteborg',166844,'est.mollis@hotmail.com','03 70 02 66 26','consectetuer','9ac73f1e7126486a9cd321d96ed065dd206a2e1f',0,'particulier',4);
/*!40000 ALTER TABLE `client` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `coefficient`
--

DROP TABLE IF EXISTS `coefficient`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `coefficient` (
  `nomination` varchar(50) NOT NULL,
  PRIMARY KEY (`nomination`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `coefficient`
--

LOCK TABLES `coefficient` WRITE;
/*!40000 ALTER TABLE `coefficient` DISABLE KEYS */;
INSERT INTO `coefficient` VALUES ('particulier'),('professionnel');
/*!40000 ALTER TABLE `coefficient` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commande`
--

DROP TABLE IF EXISTS `commande`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commande` (
  `idCom` int(11) NOT NULL,
  `dateCom` datetime NOT NULL,
  `idClient` int(11) NOT NULL DEFAULT '1',
  `reduction` int(11) DEFAULT NULL,
  `TotalTTC` decimal(5,2) DEFAULT NULL,
  `statut` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idCom`,`dateCom`),
  KEY `idClient` (`idClient`),
  CONSTRAINT `commande_ibfk_1` FOREIGN KEY (`idClient`) REFERENCES `client` (`idClient`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commande`
--

LOCK TABLES `commande` WRITE;
/*!40000 ALTER TABLE `commande` DISABLE KEYS */;
INSERT INTO `commande` VALUES (1,'2022-03-14 00:00:00',1,0,160.00,'saisie');
/*!40000 ALTER TABLE `commande` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `commercial`
--

DROP TABLE IF EXISTS `commercial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `commercial` (
  `idCommercial` int(11) NOT NULL,
  `nom` varchar(50) DEFAULT NULL,
  `prenom` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`idCommercial`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `commercial`
--

LOCK TABLES `commercial` WRITE;
/*!40000 ALTER TABLE `commercial` DISABLE KEYS */;
INSERT INTO `commercial` VALUES (1,'Goudonnet','Jonathan'),(2,'Pac','Pierre'),(3,'Raimbault','Diane'),(4,'Lopez','Kévin');
/*!40000 ALTER TABLE `commercial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contienirproduit`
--

DROP TABLE IF EXISTS `contienirproduit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `contienirproduit` (
  `RefProduit` int(11) NOT NULL,
  `reference` int(11) NOT NULL,
  PRIMARY KEY (`RefProduit`,`reference`),
  KEY `reference` (`reference`),
  CONSTRAINT `contienirproduit_ibfk_1` FOREIGN KEY (`RefProduit`) REFERENCES `produit` (`RefProduit`),
  CONSTRAINT `contienirproduit_ibfk_2` FOREIGN KEY (`reference`) REFERENCES `lignecommande` (`reference`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contienirproduit`
--

LOCK TABLES `contienirproduit` WRITE;
/*!40000 ALTER TABLE `contienirproduit` DISABLE KEYS */;
INSERT INTO `contienirproduit` VALUES (108,1),(113,2);
/*!40000 ALTER TABLE `contienirproduit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `facture`
--

DROP TABLE IF EXISTS `facture`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `facture` (
  `numeroFact` int(11) NOT NULL,
  `statut` varchar(50) DEFAULT NULL,
  `datePayement` date DEFAULT NULL,
  `idCom` int(11) NOT NULL,
  `dateCom` datetime NOT NULL,
  PRIMARY KEY (`numeroFact`),
  UNIQUE KEY `idCom` (`idCom`,`dateCom`),
  CONSTRAINT `facture_ibfk_1` FOREIGN KEY (`idCom`, `dateCom`) REFERENCES `commande` (`idCom`, `dateCom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `facture`
--

LOCK TABLES `facture` WRITE;
/*!40000 ALTER TABLE `facture` DISABLE KEYS */;
INSERT INTO `facture` VALUES (1,'impayé',NULL,1,'2022-03-14 00:00:00');
/*!40000 ALTER TABLE `facture` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `fournisseur`
--

DROP TABLE IF EXISTS `fournisseur`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `fournisseur` (
  `RefFournisseur` int(11) NOT NULL,
  `nomination` varchar(255) NOT NULL,
  PRIMARY KEY (`RefFournisseur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `fournisseur`
--

LOCK TABLES `fournisseur` WRITE;
/*!40000 ALTER TABLE `fournisseur` DISABLE KEYS */;
INSERT INTO `fournisseur` VALUES (1011998578,'LES CACTUS DE CHEZ JAMIN'),(1020014253,' Horticulture F. GOUIN\r\n'),(1032025689,' JEAN-ANDRE AUDISSOU\r\n'),(1041545526,' LEPAGE VIVACES\r\n'),(1254671256,'Ets. Kuentz');
/*!40000 ALTER TABLE `fournisseur` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `historique`
--

DROP TABLE IF EXISTS `historique`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `historique` (
  `idClient` int(11) NOT NULL,
  `idCom` int(11) NOT NULL,
  `dateCom` datetime NOT NULL,
  PRIMARY KEY (`idClient`,`idCom`,`dateCom`),
  KEY `idCom` (`idCom`,`dateCom`),
  CONSTRAINT `historique_ibfk_1` FOREIGN KEY (`idClient`) REFERENCES `client` (`idClient`),
  CONSTRAINT `historique_ibfk_2` FOREIGN KEY (`idCom`, `dateCom`) REFERENCES `commande` (`idCom`, `dateCom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `historique`
--

LOCK TABLES `historique` WRITE;
/*!40000 ALTER TABLE `historique` DISABLE KEYS */;
/*!40000 ALTER TABLE `historique` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `lignecommande`
--

DROP TABLE IF EXISTS `lignecommande`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `lignecommande` (
  `reference` int(11) NOT NULL,
  `designation` varchar(50) DEFAULT NULL,
  `prixHT` decimal(5,2) DEFAULT NULL,
  `quantite` int(11) DEFAULT NULL,
  `idCom` int(11) NOT NULL,
  `dateCom` datetime NOT NULL,
  PRIMARY KEY (`reference`),
  KEY `idCom` (`idCom`,`dateCom`),
  CONSTRAINT `lignecommande_ibfk_1` FOREIGN KEY (`idCom`, `dateCom`) REFERENCES `commande` (`idCom`, `dateCom`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `lignecommande`
--

LOCK TABLES `lignecommande` WRITE;
/*!40000 ALTER TABLE `lignecommande` DISABLE KEYS */;
INSERT INTO `lignecommande` VALUES (1,'Kit jardin ',30.00,1,1,'2022-03-14 00:00:00'),(2,'Palmm',130.00,1,1,'2022-03-14 00:00:00');
/*!40000 ALTER TABLE `lignecommande` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `livraison`
--

DROP TABLE IF EXISTS `livraison`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `livraison` (
  `idLiv` int(11) NOT NULL,
  `dateLivraison` date DEFAULT NULL,
  `bonlivraison` varchar(50) DEFAULT NULL,
  `numeroFact` int(11) NOT NULL,
  PRIMARY KEY (`idLiv`),
  KEY `numeroFact` (`numeroFact`),
  CONSTRAINT `livraison_ibfk_1` FOREIGN KEY (`numeroFact`) REFERENCES `facture` (`numeroFact`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `livraison`
--

LOCK TABLES `livraison` WRITE;
/*!40000 ALTER TABLE `livraison` DISABLE KEYS */;
/*!40000 ALTER TABLE `livraison` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `produit`
--

DROP TABLE IF EXISTS `produit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `produit` (
  `RefProduit` int(11) NOT NULL,
  `shortLibel` varchar(100) DEFAULT NULL,
  `src` varchar(500) NOT NULL,
  `srcVid` varchar(500) DEFAULT NULL,
  `reference` int(11) DEFAULT NULL,
  `longLibel` varchar(500) DEFAULT NULL,
  `prxAchat` decimal(5,2) NOT NULL,
  `idCat` int(11) NOT NULL,
  PRIMARY KEY (`RefProduit`),
  KEY `idCat` (`idCat`),
  CONSTRAINT `produit_ibfk_1` FOREIGN KEY (`idCat`) REFERENCES `categorie` (`idCat`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `produit`
--

LOCK TABLES `produit` WRITE;
/*!40000 ALTER TABLE `produit` DISABLE KEYS */;
INSERT INTO `produit` VALUES (100,'Dalhia jaune','https://www.visoflora.com/images/original/dahlia-jaune-visoflora-36606.jpg',NULL,NULL,'Dalhia Imperium couleur jaune a planter soi même. Prix du bulbe à l\'unitée.',3.50,6),(101,'Cactus Grande Taille','https://images.pexels.com/photos/4507263/pexels-photo-4507263.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260','../images/plant006_vid.mp4',NULL,'Cactus d\'interieur/exterieur 26cm de haut',26.00,5),(102,'Presentoir interieur','../images/plant009.png','../images/plant_009_vid.mp4',NULL,'décorez votre intérieur avec ce présentoir modern pour plantes. Pots compris dans l\'offre cactus non inclus.',10.00,8),(103,'Espostoa','../images/plant002.png',NULL,NULL,'Espostoa est un genre de la famille des cactus composé de seize espèces.Ce sont des cierges arbustifs ou arborescents couverts d\'épines denses et portant souvent des poils ou des cheveux blancs. Seuls les spécimens âgés peuvent se ramifier.',6.99,6),(104,'Asplenium antiquum','../images/plant001.jpeg','../images/plante001_v2.jpg',NULL,'Ces fougères prospèrent bien aux températures habituelles d\'une pièce d\'habitation. N\'exposez pas Asplenium nidus à des températures inférieures à 15° en hiver et les autres espèces, au-dessous de 10°.',32.50,6),(105,'Cisaille à haie','../images/CisailleHaie.jpeg',NULL,NULL,'Cisaille à haies spécialement conçue pour l\'usage professionnel en espaces verts, domaines viticoles et particulièrement pour l\'art topiaire\r\nLame entièrement trempée\r\nLame crantée pour la coupe de grosses branches, jusqu\'à 10 mm de diamètre',58.00,7),(106,'Petit Kit de jardinage','../images/KitOutils.jpeg','../images/KitOutils_V2.jpeg',NULL,'Kit de jardinage pour débutants.Contenant une petite pelle, une griffe et une serfouette avec poignée en bois ergonomique, vous pourrez réaliser vos travaux de jardin tout en profitant d\'un outillage de qualité vous offrant un réél confort d\'utilisation.',25.00,7),(107,'Cisaille à main','https://images.pexels.com/photos/5027604/pexels-photo-5027604.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260','../images/cisailleMain.mp4',NULL,'Coupez tout type de haies avec l\'aide de cette cisaille Bahco P59-25-F universelle.\r\n\r\nRobuste et fiable, elle peut être utilisée régulièrement afin d\'entretenir votre jardin avec efficacité.\r\n\r\nCet outil possède une lame en acier trempé et des butée en caoutchouc afin de travailler sans difficulté.',33.25,7),(108,'Kit jardin ','https://images.pexels.com/photos/5218008/pexels-photo-5218008.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',NULL,NULL,'Les enfants sont naturellement très ouverts et curieux de découvrir la magie de la nature. Pour les aider à se lancer dans une activité de jardinage, voici le kit idéal pour les équiper d\'outils de jardinage adaptés à leur morphologie.',30.00,7),(109,'Roses Rouges','https://images.pexels.com/photos/1252812/pexels-photo-1252812.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260','../images/rose_VId.mp4',NULL,'Ncient 50 pcs/Sac Graines Semences de Rose Couleur Rouge de Graines Fleurs Graines à Planter Plante Rare de Jardin Balcon Vivaces Belle Floraison Bonsaï en Plein Air pour l\'Intérieur et l\'Extérieur',10.99,6),(110,'Nopal','../images/plant006.jpeg',NULL,NULL,'Nopal est un nom commun en espagnol pour les cactus Opuntia, ainsi que pour ses coussinets. Il existe environ 114 espèces connues au Mexique',70.00,6),(111,'Epipremnum aureum','../images/plant004.jpeg','https://media.istockphoto.com/videos/rain-on-evening-video-id1329649492',NULL,'La plante Epipremnum aureum est très populaire dans les régions tempérées pour sa facilité de culture qui ne demande quasiment aucun entretien',13.00,5),(112,'Pot noir modern','https://images.pexels.com/photos/4273435/pexels-photo-4273435.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=750&w=1260',NULL,NULL,'Pot moderne noir pour plante d\'interieur',11.50,8),(113,'Palm','https://img.made.com/image/upload/c_pad,d_madeplusgrey.svg,f_auto,w_982,dpr_2.0,q_auto:good,b_rgb:f5f6f4/v4/catalog/product/asset/c/1/c/9/c1c92985f59ad81056038380c2d1d99b8779e66b_IACPAL003GRE_UK_Palm_Set_Two_Large_Earthenware_Plant_Stands_Sage_Pink_ar3_2_LB01_PS.png','https://img.made.com/image/upload/c_pad,d_madeplusgrey.svg,f_auto,w_982,dpr_2.0,q_auto:good,b_rgb:f5f6f4/v4/catalog/product/asset/d/5/6/1/d561e91f7e9ef48e425a93a7bb4a09fed766b554_IACPAL003GRE_UK_Palm_Set_Two_Large_Earthenware_Plant_Stands_Sage_Pink_ar3_2_LB02_LS.jpg',NULL,'Lot de 2 grands pots de fleurs sur pieds, vert sauge et rose',130.00,8),(114,'Umbra Trigg Jardinière Suspendue','https://m.media-amazon.com/images/I/71zFkdcesJL._AC_SX679_.jpg','https://m.media-amazon.com/images/I/71-kaFXpyxL._AC_SX679_.jpg',NULL,'SUPERBE DESIGN MODERNE ET GEOMETRIQUE : Trigg est un pot de fleur mural avec support géométrique original, à la fois simple et élégant.\r\nMAGNIFIQUE POT DE FLEURS : Trigg est la solution idéale pour introduire de petites plantes d\'intérieur, telles que des plantes succulentes, des plantes aériennes et autres chez vous ou au bureau\r\nLÉGER ET POLYVALENT: Trigg peut également être utilisé pour stocker de petits objets comme des stylos, des crayons et même des accessoires de maquillage',24.00,8),(115,'Donec molestie','Phasellus tristique mauris sed','volutpat malesuada. Mauris ',NULL,'aliquam scelerisque urna at mattis. Donec molestie magna leo, sed eleifend est mollis id',8.99,6),(116,'Terrarium en verre forme géométrique','https://m.media-amazon.com/images/I/91tfQ65HLPL._AC_SX679_.jpg','https://m.media-amazon.com/images/I/71Qddbj2AbL._AC_SX679_.jpg',NULL,'Style moderne terrarium avec élégance classique, un joli cube en verre artistique d\'une pièce de votre maison ou votre terrasse.Taille idéale pour la fougère, mousse, succulente, airplants, cactus ou d\'autres plantes avec facile maintainence',41.25,8),(117,'Cache-pot sur pied','https://cdn.laredoute.com/products/0/e/1/0e1723de8770ff303c1e64ad7acc2a2c.jpg?imgopt=twic&twic=v1/cover=700x700','https://cdn.laredoute.com/products/7/d/d/7dd10c78a7c937eaaf38a0a41d0bbadc.jpg?imgopt=twic&twic=v1/cover=700x700',NULL,'2 cache-pots en métal. Chacun doté de son piédestal, En raison du processus de production, il y a un petit trou de quelques mm de taille sur le bord supérieur.',66.00,8),(118,'Euphorbia ammak','../images/plant010.jpeg','../images/plant010_vid.mp4',NULL,'Cactus exterieur grande taille. 38cm environ. ',40.00,6),(119,'Voile d\'hivernage','https://photos.gammvert.fr/v5/products/fiche_590/52281-voile-dhivernage-nortene-wintertex-2-x-5-m-2.jpg',NULL,NULL,'Ce voile d’hivernage de la marque Nortene protège les plantes du froid et du vent. Il est conçu pour tous types de plantes. Son voile est perméable à l\'eau et à l\'air. Il est un bon isolant thermique. Il est également facile à mettre en place.\r\n',15.00,6),(120,'Voile d\'hivernage','https://photos.gammvert.fr/v5/products/fiche_590/52286-voile-dhivernage-nortene-hivertex-2-x-5-m-2.jpg','https://photos.gammvert.fr/v5/products/fiche_590/52288-voile-dhivernage-nortene-hivertex-4-x-6-m-3.jpg',NULL,'Ce voile d’hivernage Hivertex de la marque Nortene est un voile épais pour une protection renforcée contre le froid. Il est perméable à l’eau et à l’air. Ainsi, vous pourrez protéger vos plantes du froid l’hiver.',19.00,6),(121,'Coupe branches télescopique','https://photos.gammvert.fr/v5/products/fiche_590/50626-coupe-branches-multifonctions-upx86-scie-fiskars-3.JPG','https://photos.gammvert.fr/v5/products/fiche_590/50626-coupe-branches-multifonctions-upx86-scie-fiskars-6.jpg',NULL,'Diamètre de coupe : Ø 32 mm\r\nPortée de coupe d\'environ 3,5 m\r\nTête de coupe réglable jusqu\'à 230°',109.00,7),(122,'Palmier de Madagascar','../images/palmierMadagascar.jpeg',NULL,NULL,'Palmier en pot 1.20m ',100.00,6);
/*!40000 ALTER TABLE `produit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `stocker`
--

DROP TABLE IF EXISTS `stocker`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `stocker` (
  `RefProduit` int(11) NOT NULL,
  `RefFournisseur` int(11) NOT NULL,
  `quantite` int(11) DEFAULT NULL,
  PRIMARY KEY (`RefProduit`,`RefFournisseur`),
  KEY `RefFournisseur` (`RefFournisseur`),
  CONSTRAINT `stocker_ibfk_1` FOREIGN KEY (`RefProduit`) REFERENCES `produit` (`RefProduit`),
  CONSTRAINT `stocker_ibfk_2` FOREIGN KEY (`RefFournisseur`) REFERENCES `fournisseur` (`RefFournisseur`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `stocker`
--

LOCK TABLES `stocker` WRITE;
/*!40000 ALTER TABLE `stocker` DISABLE KEYS */;
INSERT INTO `stocker` VALUES (100,1020014253,30),(101,1011998578,50),(102,1254671256,100),(103,1011998578,50),(104,1032025689,60),(105,1020014253,14),(106,1041545526,25),(107,1020014253,10),(108,1032025689,20),(109,1041545526,105),(110,1254671256,80),(111,1011998578,70),(112,1254671256,30),(113,1020014253,20),(114,1254671256,54),(115,1041545526,60),(116,1032025689,35),(117,1254671256,50),(118,1032025689,20),(119,1011998578,200),(120,1020014253,150),(121,1020014253,30),(122,1254671256,25);
/*!40000 ALTER TABLE `stocker` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-03-14 15:08:37
